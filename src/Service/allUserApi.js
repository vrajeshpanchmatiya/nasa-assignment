import axios from "axios";
// Api calling with help of Axios
export const allUserApi = () => {
  return axios.get(`${process.env.REACT_APP_API_URL_ALL}`);
};
