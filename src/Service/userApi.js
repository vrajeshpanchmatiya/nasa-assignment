import axios from "axios";
// Api calling with the help of Axios
export const userApi = (id) => {
  return axios.get(
    `${process.env.REACT_APP_API_URL_USER}${id}?${process.env.REACT_APP_API_URL_KEY}`
  );
};
