import { allUserApi } from "../Service/allUserApi";
export const allUserAction = async () => {
  const detail = await allUserApi();
  return detail.data;
};
