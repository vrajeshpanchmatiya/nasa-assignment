import { userApi } from "../Service/userApi";
import { userType } from "./Type/userType";
export const userAction = (id) => {
  return async (dispatch) => {
    // userApi calling and dispatch
    const details = await userApi(id);
    dispatch({ type: userType, payload: details.data });
  };
};
