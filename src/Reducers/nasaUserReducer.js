import { userType } from "../Actions/Type/userType";
// initialState value of state
const initialState = {
  data: [],
};
// Reducer for store
export const nasaUserReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case userType:
      return {
        ...state,
        data: payload,
      };
    default:
      return state;
  }
};
