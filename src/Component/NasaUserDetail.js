import { Box, Typography } from "@material-ui/core";
import React from "react";
import { useSelector } from "react-redux";
import "../Common.scss";
const NasaUserDetail = () => {
  // useSelector for calling user data
  const info = useSelector((state) => {
    return state.data;
  });
  return (
    <div className="di">
      <Box className="bx">
        <h1>Nasa User Detail</h1>
        <Typography>
          <b>Name: </b>
          {info?.name}
        </Typography>
        <Typography>
          <b>URL: </b>
          {info?.nasa_jpl_url}
        </Typography>
        <Typography>
          <b>Potentially Hazardeous Asteroid: </b>
          {info?.is_potentially_hazardeous_asteroid ? "true" : "false"}
        </Typography>
      </Box>
    </div>
  );
};
export default NasaUserDetail;
