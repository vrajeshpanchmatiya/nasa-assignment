import { Box, Button, Paper, TextField } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { userAction } from "../Actions/userAction";
import { Link } from "react-router-dom";
import { allUserAction } from "../Actions/allUserAction";
import "../Common.scss";
const NasaUserForm = () => {
  const [id, setId] = useState(null);
  const [allAsteroid, setAllAsteroid] = useState([]);
  const dispatch = useDispatch();
  const changeId = (e) => {
    setId(e.target.value);
  };
  // Handle User Id when submit button
  const handleUser = () => {
    dispatch(userAction(id));
  };
  // useEffect for calling All User
  useEffect(() => {
    const fetch = async () => {
      const data = await allUserAction();

      setAllAsteroid(data.near_earth_objects);
    };
    fetch();
  }, []);
  // handle Random User for dispatch
  const handleRandomUser = () => {
    const data = allAsteroid.map(({ id }) => id);
    const number = Math.ceil(Math.abs(Math.random() * data.length));
    dispatch(userAction(data[number]));
  };
  // Nasa User Form
  return (
    <div className="di">
      <Box className="bx">
        <h1>Nasa User Form</h1>

        <TextField
          name={id}
          label="Asteroid Id"
          onChange={changeId}
          variant="outlined"
          color="primary"
        />
        <Link
          to={{ pathname: "/NasaUserDetail" }}
          style={{ textDecoration: "none" }}
        >
          <Button
            type="submit"
            onClick={handleUser}
            variant="outlined"
            disabled={id === null || id.length <= 5}
            color="primary"
          >
            Submit
          </Button>
        </Link>
        <Link
          to={{ pathname: "/NasaUserDetail" }}
          style={{ textDecoration: "none" }}
        >
          <Button
            type="submit"
            onClick={handleRandomUser}
            variant="outlined"
            color="primary"
          >
            Fetch Random User
          </Button>
        </Link>
      </Box>
    </div>
  );
};
export default NasaUserForm;
